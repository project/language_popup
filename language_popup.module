<?php

/**
 * @file
 * Language popup module file.
 * This will add a popup option selection in language detection and selection.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrlFallback;
use Drupal\language_popup\Plugin\LanguageNegotiation\LanguageNegotiationPopup;

/**
 * Implements hook_help().
 */
function language_popup_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.language_popup':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module provides an additional option in language negotiation features. <br/> This module provides an option to select a language to traverse if there is no language present in URL.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Managing modules') . '</dt>';
      $output .= '<dd>' . t('Users with appropriate permission can install and uninstall modules from the <a href="#">Extend page</a>.');
      $output .= '<dt>' . t('Configuring the module') . '</dt>';
      $output .= '<dd>' . t('Once module enabled / installed from module page or command line, you can go to language detection and selection page to configure newly added language popup option.') . '</dd>';
      $output .= '</dl>';
    return $output;
  }
}

/**
 * Alters hook_language_types_info().
 *
 * For determining the URL language, the cookie language method should take
 * precedence over the URL fallback method.
 *
 * The first method (LanguageNegotiationUrl) attempts to get the
 * language to use for links in the page from the path prefix or the domain.
 * If no language is found there, instead of falling back to the
 * LanguageNegotiationUrlFallback method (which would return the
 * site default language), we attempt to get the language from the cookie first.
 *
 * @see language_language_types_info_alter()
 * @see https://drupal.org/node/1497272
 */
function language_popup_language_types_info_alter(&$language_types) {
  $language_types[LanguageInterface::TYPE_URL]['fixed'] = [
    LanguageNegotiationUrl::METHOD_ID,
    LanguageNegotiationPopup::METHOD_ID,
    LanguageNegotiationUrlFallback::METHOD_ID,
  ];
}

/**
 * Implements hook_page_attachments().
 */
function language_popup_page_attachments(array &$page) {
  global $base_url;

  $language_popup_value = \Drupal::state()->get('language_popup_set');
  if ($language_popup_value) {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $page['#attached']['library'][] = 'language_popup/language_popup_js';
    $language_list = \Drupal::languageManager()->getLanguages();
    $languages = array();
    foreach ($language_list as $key => $value) {
      $languages[$base_url . '/' . $key] = $value->getName();
    }

    $page['#attached']['drupalSettings'] = [
      'popup_heading' => \Drupal::config('language_popup.negotiation')->get('language_popup.heading'),
      'language_list' => $languages
    ];
  }
}
