<?php

namespace Drupal\language_popup\Form;

use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NegotiationLanguagePopupForm extends ConfigFormBase {

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'language_popup_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['language_popup.negotiation'];
  }

    /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->config = $this->config('language_popup.negotiation');

    $language_list = \Drupal::languageManager()->getLanguages();
    $defaultLanguage = \Drupal::languageManager()->getDefaultLanguage();

    $languages = array();
    foreach ($language_list as $key => $value) {
      $languages[$key] = $value->getName();
    }

    $defaultValue = $this->config->get('language_popup.default_redirect') ? $this->config->get('language_popup.default_redirect') : $defaultLanguage->getId();

    $form['language_popup_default_redirect'] = array(
      '#title' => $this->t('Language default redirect'),
      '#type' => 'select',
      '#options' => $languages,
      '#default_value' => $defaultValue,
      '#description' => $this->t('Language redirects other than default language.'),
    );

    $form['language_popup_heading'] = array(
      '#title' => $this->t('Popup heading'),
      '#type' => 'textfield',
      '#default_value' => $this->config->get('language_popup.heading'),
      '#description' => $this->t('Popup heading text that displays in popup.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config = $this->config('language_popup.negotiation');

    if ($form_state->hasValue('language_popup_default_redirect')) {
      $this->config->set('language_popup.default_redirect', $form_state->getValue('language_popup_default_redirect'));
    }

    if ($form_state->hasValue('language_popup_heading')) {
      $this->config->set('language_popup.heading', $form_state->getValue('language_popup_heading'));
    }

    $this->config->save();
  }

}
