<?php

namespace Drupal\language_popup\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\language\LanguageNegotiatorInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationSelected;
use Drupal\language_popup\Plugin\LanguageNegotiation\LanguageNegotiationPopup;

/**
 * Provides a LanguageCookieSubscriber.
 */
class LanguagePopupSubscriber implements EventSubscriberInterface {

  /**
   * The event.
   *
   * @var \Symfony\Component\HttpKernel\Event\ResponseEvent
   */
  protected $event;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language negotiator.
   *
   * @var \Drupal\language\LanguageNegotiatorInterface
   */
  protected $languageNegotiator;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new class object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\language\LanguageNegotiatorInterface $language_negotiator
   *   The language negotiator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, LanguageNegotiatorInterface $language_negotiator, ModuleHandlerInterface $module_handler) {
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
    $this->languageNegotiator = $language_negotiator;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Helper method that gets the language code to set the cookie to.
   *
   * Loops through all available language negotiation methods with higher
   * priority than the Language Cookie method itself.
   *
   * @see \Drupal\language_cookie\LanguageCookieSubscriber::setLanguageCookie()
   *
   * @return string|bool
   *   An string with the language code or FALSE.
   */
  protected function getLanguage() {
    if (!$this->languageManager->isMultilingual()) {
      return $this->languageManager->getDefaultLanguage()->getId();
    }

    // Get all methods available for this language type.
    $methods = $this->languageNegotiator->getNegotiationMethods();

    // We ignore this language method or else it will always return a language.
    unset($methods[LanguageNegotiationSelected::METHOD_ID]);
    uasort($methods, 'Drupal\Component\Utility\SortArray::sortByWeightElement');

    foreach ($methods as $method_id => $method_definition) {
      // Do not consider language providers with a lower priority than the
      // cookie language provider, nor the cookie provider itself.
      if ($method_id == LanguageNegotiationPopup::METHOD_ID) {
        return FALSE;
      }

      $lang = $this->languageNegotiator->getNegotiationMethodInstance($method_id)->getLangcode($this->event->getRequest());

      if ($lang) {
        return $lang;
      }
    }

    // If no other language was found, use the default one.
    return $this->languageManager->getDefaultLanguage()->getId();
  }

  /**
   * Event callback for setting the language cookie.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   *
   * @return bool
   *   - FALSE if language is already set.
   *   - TRUE if language is not set & need to be set using popup.
   */
  public function setLanguagePopup(\Symfony\Component\HttpKernel\Event\ResponseEvent $event) {
    $this->event = $event;
    $config = $this->configFactory->get('language_popup.negotiation');

    if ($lang = $this->getLanguage()) {
      \Drupal::state()->set('language_popup_set', FALSE);
      return FALSE;
    }

    \Drupal::state()->set('language_popup_set', FALSE);
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = array('setLanguagePopup', 30);
    return $events;
  }

}
