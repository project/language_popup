<?php

namespace Drupal\language_popup\Plugin\LanguageNegotiation;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class for identifying language from a language popup.
 *
 * @LanguageNegotiation(
 *   weight = -6,
 *   name = @Translation("Language Popup"),
 *   description = @Translation("Determine the language from a selection popup"),
 *   id = Drupal\language_popup\Plugin\LanguageNegotiation\LanguageNegotiationPopup::METHOD_ID,
 *   config_route_name = "language_popup.negotiation_popup"
 * )
 */
class LanguageNegotiationPopup extends LanguageNegotiationMethodBase implements ContainerFactoryPluginInterface {

  /**
   * The language negotiation method ID.
   */
  const METHOD_ID = 'language-popup';

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new LanguageNegotiationPopup instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $config = $this->configFactory->get('language_popup.negotiation');
    $popupDefaultLanguage = $config->get('language_popup.default_redirect');

    \Drupal::state()->set('language_popup_set', $popupDefaultLanguage);
    \Drupal::service('page_cache_kill_switch')->trigger();
    return $popupDefaultLanguage;
  }

}
