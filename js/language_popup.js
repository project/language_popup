(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.LanguagePopup = {
    attach: function (context, settings) {
      var content  = '<div id="popup-message-content">';
      content = content + '<div id="language-popup-list"><ul>';
      $.each(drupalSettings.language_list, function(i, val) {
        content += '<li><a href=' + i + '>' + val + '</a></li>';
      });
      content = content + '</ul></div></div>';
      var confirmationDialog = Drupal.dialog(content, {
        dialogClass: 'confirm-dialog',
        resizable: true,
        closeOnEscape: true,
        width: 400,
        title:drupalSettings.popup_heading,
        beforeClose: false,
        close: function (event) {
          $(event.target).remove();
        }
      });

      $(window).on('load', function(event) {
        event.preventDefault();
        confirmationDialog.showModal();
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
