CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Usage
 * Support
 * Compatibility
 * Authors


INTRODUCTION
------------

Adds an extra "Popup" option in the Language Negotiation settings, allowing the
a user can select the language listed in popup.

The default content redirection & message title are configurable in
the settings page.


USAGE
------

- Enable the module and go to: Administration » Configuration » Regional and
  language » Languages » Detection and selection (available at the following
  URL: 'admin/config/regional/language/configure').

- Enable the "popup" detection method and re-arrange the user interface language
  detection.

- Once the user visited the site he can see the language selection popup
  so that user will select the language he/she wants to traverse throught
  the site.

- This requires URL options to be selected for path prefix only.

- We are working on doamin selection. It will be available in next release.


SUPPORT
-------

If you find a bug or have a feature request please file an issue:

http://drupal.org/node/add/project-issue/language_popup


COMPATIBILITY
-------------

Module is compatible with all other module. If you find a bug please use
the issue tracker for support. Thanks!


AUTHORS
--------

* Ravi Mane (ravimane23)
  http://drupal.org/user/2616789
